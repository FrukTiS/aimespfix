#include <amxmodx>

#define REHLDS_VERSION

#if defined REHLDS_VERSION
	#include <reapi>
	#define RETURN_HOOK	0
#else
	#include <hamsandwich>
	#define RETURN_HOOK	1
#endif

#define VIP_ENABLE		123
//#define VIP_DISABLE	321
#define FLAG_ACCESS	ADMIN_LEVEL_H

new Msg_ScoreAttrib;

public plugin_init()
{

#if defined REHLDS_VERSION
	register_plugin("[ReAPI] AimEspFix VIP Tab", "1.0", "?");
	RegisterHookChain(RG_CBasePlayer_Spawn, "CBasePlayer_Spawn", true);
#else
	register_plugin("AimEspFix VIP Tab", "1.0", "?");
	RegisterHam(Ham_Spawn, "player", "CBasePlayer_Spawn", true);
#endif

	Msg_ScoreAttrib = get_user_msgid("ScoreAttrib");
}

public CBasePlayer_Spawn(id)
{
	if(!(get_user_flags(id) & FLAG_ACCESS))
		return RETURN_HOOK;

	if(is_user_alive(id))
	{
		emessage_begin(MSG_ALL, Msg_ScoreAttrib);
		ewrite_byte(id);
		ewrite_byte(VIP_ENABLE);
		emessage_end();
	}

	return RETURN_HOOK;
}