IFS='.:' read ip1 ip2 ip3 ip4 port <<< $1
	g++ -O3 -m32 -g0 -s -shared -Wno-write-strings -fPIC -fno-rtti -fno-exceptions -std=c++11 \
	-fno-stack-protector -msse2 -fomit-frame-pointer -funroll-loops -mtune=generic \
	-fvisibility=hidden -fvisibility-inlines-hidden -Dlinux -D__linux__ -D_stricmp=strcasecmp -D_vsnprintf=vsnprintf \
	-DOCTET_1=$ip1 -DOCTET_2=$ip2 -DOCTET_3=$ip3 -DOCTET_4=$ip4 -DOCTET_5=$port \
	-I. -Isdk/cssdk/common -Isdk/cssdk/dlls -Isdk/cssdk/engine -Isdk/cssdk/game_shared -Isdk/cssdk/pm_shared -Isdk/cssdk/public -Isdk/metamod \
	src/Main.cpp src/Config.cpp src/Memory.cpp src/Meta_api.cpp \
	-lrt -ldl -lm \
	-o aimespfix_mm_i386.so
