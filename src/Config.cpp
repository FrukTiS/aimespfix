#include "Main.h"

#define CONFIG_FILE /*config.cfg*/XorStr<0x79,11,0xB9AC1570>("\x1A\x15\x15\x1A\x14\x19\x51\xE3\xE7\xE5"+0xB9AC1570).s

char* trimbuf(char *str)
{
	if (str == nullptr) {
		return nullptr;
	}

	char *ibuf = str;
	int i = 0;

	for (ibuf = str; *ibuf && (byte)(*ibuf) < (byte)0x80 && isspace(*ibuf); ++ibuf)
		;

	i = strlen(ibuf);

	if (str != ibuf) {
		memmove(str, ibuf, i);
	}

	str[i] = 0;

	while (--i >= 0)
	{
		if (!isspace(ibuf[i])) {
			break;
		}
	}

	ibuf[++i] = 0;

	return str;
}

void Print_Settings()
{
	printf(/*\n\nusage: aimespfix_option\n\n [command] [value]\n\n*/XorStr<0xC5, 48, 0xEDE9955C>("\xCF\xCC\xB2\xBB\xA8\xAD\xAE\xF6\xED\xAF\xA6\xBD\xB4\xA1\xA3\xB2\xBC\xAE\x88\xB7\xA9\xAE\xB2\xB3\xB3\xD4\xD5\xC0\xBA\x81\x8C\x89\x88\x87\x89\x8C\xB4\xCA\xB0\x9A\x8C\x82\x9A\x95\xAC\xF8\xF9" + 0xEDE9955C).s);
	printf(/*aim_ffa %d\n*/XorStr<0x90, 12, 0xF95624FE>("\xF1\xF8\xFF\xCC\xF2\xF3\xF7\xB7\xBD\xFD\x90" + 0xF95624FE).s, g_AimEspFixCfg.AimFFA);
	printf(/*aim_tab_fix %d\n*/XorStr<0x4F, 16, 0xCD1AE1A7>("\x2E\x39\x3C\x0D\x27\x35\x37\x09\x31\x31\x21\x7A\x7E\x38\x57" + 0xCD1AE1A7).s, g_AimEspFixCfg.AimTabFix);
	printf(/*aim_tab_flashed_fix %d\n*/XorStr<0x01, 24, 0x03C40576>("\x60\x6B\x6E\x5B\x71\x67\x65\x57\x6F\x66\x6A\x7F\x65\x6B\x6B\x4F\x77\x7B\x6B\x34\x30\x72\x1D" + 0x03C40576).s, g_AimEspFixCfg.AimTabFix);
	printf(/*snd_active %d\n*/XorStr<0x10, 15, 0x222EB6FB>("\x63\x7F\x76\x4C\x75\x76\x62\x7E\x6E\x7C\x3A\x3E\x78\x17" + 0x222EB6FB).s, g_AimEspFixCfg.SndActive);
	printf(/*snd_count %d\n*/XorStr<0xBE, 14, 0xEA6E88E4>("\xCD\xD1\xA4\x9E\xA1\xAC\xB1\xAB\xB2\xE7\xED\xAD\xC0" + 0xEA6E88E4).s, g_AimEspFixCfg.SndCount);
	printf(/*snd_scalez %d\n*/XorStr<0xCB, 15, 0x632A750C>("\xB8\xA2\xA9\x91\xBC\xB3\xB0\xBE\xB6\xAE\xF5\xF3\xB3\xD2" + 0x632A750C).s, g_AimEspFixCfg.SndScalez);
	printf(/*snd_radius %d\n*/XorStr<0x10, 15, 0x3A87778A>("\x63\x7F\x76\x4C\x66\x74\x72\x7E\x6D\x6A\x3A\x3E\x78\x17" + 0x3A87778A).s, g_AimEspFixCfg.SndRadius);
	printf(/*snd_volume %d\n*/XorStr<0x08, 15, 0x21EBB275>("\x7B\x67\x6E\x54\x7A\x62\x62\x7A\x7D\x74\x32\x36\x70\x1F" + 0x21EBB275).s, g_AimEspFixCfg.SndVolume);
	printf(/*snd_only_player %d\n\n*/XorStr<0xDD, 21, 0xA288AD52>("\xAE\xB0\xBB\xBF\x8E\x8C\x8F\x9D\xBA\x96\x8B\x89\x90\x8F\x99\xCC\xC8\x8A\xE5\xFA" + 0xA288AD52).s, g_AimEspFixCfg.SndOnlyPlayer);
}

void AEF_PRINT(const char *fmt, ...)
{
	va_list ap;
	uint32 len;
	char buf[1048];

	va_start(ap, fmt);
	vsnprintf(buf, sizeof(buf), fmt, ap);
	va_end(ap);

	len = strlen(buf);

	if (len < sizeof(buf) - 2) {
		strcat(buf, "\n");
	} else {
		buf[len - 1] = '\n';
	}

	SERVER_PRINT(buf);
}

bool ParseCfgParam(const char* param, const char* value)
{

#define AEF_CFG_PARSE_INT(paramName, field, _type, minVal, maxVal)           \
	if (!_stricmp(paramName, param)) {                                     \
		int i = atoi(value);                                                 \
		if (i < minVal || i > maxVal) {                                      \
		    AEF_PRINT(/*%s Invalid %s value '%s'*/XorStr<0x39,25,0xA93035CD>("\x1C\x49\x1B\x75\x53\x48\x5E\x2C\x28\x26\x63\x61\x36\x66\x31\x29\x25\x3F\x2E\x6C\x6A\x6B\x3C\x77"+0xA93035CD).s, LOG_PREFIX, param, value); \
			return false;                                                    \
		}                                                                    \
		field = (_type) i;                                                   \
		return true;                                                         \
	}

#define AEF_CFG_PARSE_BOOL(paramName, field)                                 \
	if (!_stricmp(paramName, param)) {                                     \
		int i = atoi(value);                                                 \
		if (i < 0 || i > 1) {                                                \
		    AEF_PRINT(/*%s Invalid %s value '%s'*/XorStr<0x51,25,0x9830B67B>("\x74\x21\x73\x1D\x3B\x20\x36\x34\x30\x3E\x7B\x79\x2E\x7E\x29\x01\x0D\x17\x06\x44\x42\x43\x14\x4F"+0x9830B67B).s, LOG_PREFIX, param, value); \
			return false;                                                    \
		}                                                                    \
		field = i ? true : false;                                            \
		return true;                                                         \
	}

	AEF_CFG_PARSE_BOOL((const char *)(/*aim_ffa*/XorStr<0xD4, 8, 0xA3C6BDA9>("\xB5\xBC\xBB\x88\xBE\xBF\xBB" + 0xA3C6BDA9).s), g_AimEspFixCfg.AimFFA);
	AEF_CFG_PARSE_BOOL((const char *)(/*aim_tab_fix*/XorStr<0xCB, 12, 0x75A2678B>("\xAA\xA5\xA0\x91\xBB\xB1\xB3\x8D\xB5\xBD\xAD" + 0x75A2678B).s), g_AimEspFixCfg.AimTabFix);
	AEF_CFG_PARSE_BOOL((const char *)(/*aim_tab_flashed_fix*/XorStr<0x27, 20, 0x6E84CD16>("\x46\x41\x44\x75\x5F\x4D\x4F\x71\x49\x5C\x50\x41\x5B\x51\x51\x69\x51\x51\x41" + 0x6E84CD16).s), g_AimEspFixCfg.AimTabFlashedFix);
	AEF_CFG_PARSE_BOOL((const char *)(/*snd_active*/XorStr<0xA5, 11, 0x10D21F69>("\xD6\xC8\xC3\xF7\xC8\xC9\xDF\xC5\xDB\xCB" + 0x10D21F69).s), g_AimEspFixCfg.SndActive);
	AEF_CFG_PARSE_INT((const char *)(/*snd_count*/XorStr<0x65, 10, 0xF2AC7319>("\x16\x08\x03\x37\x0A\x05\x1E\x02\x19" + 0xF2AC7319).s), g_AimEspFixCfg.SndCount, int, 1, 10);
	AEF_CFG_PARSE_INT((const char *)(/*snd_scalez*/XorStr<0x31, 11, 0x1ED9C4BB>("\x42\x5C\x57\x6B\x46\x55\x56\x54\x5C\x40" + 0x1ED9C4BB).s), g_AimEspFixCfg.SndScalez, int, 1, 10);
	AEF_CFG_PARSE_INT((const char *)(/*snd_radius*/XorStr<0xF8, 11, 0x3C62BCDD>("\x8B\x97\x9E\xA4\x8E\x9C\x9A\x96\x75\x72" + 0x3C62BCDD).s), g_AimEspFixCfg.SndRadius, int, 64, 640);
	AEF_CFG_PARSE_INT((const char *)(/*snd_volume*/XorStr<0xB3, 11, 0x451A04F4>("\xC0\xDA\xD1\xE9\xC1\xD7\xD5\xCF\xD6\xD9" + 0x451A04F4).s), g_AimEspFixCfg.SndVolume, int, 0, 255);
	AEF_CFG_PARSE_BOOL((const char *)(/*snd_only_player*/XorStr<0xC1, 16, 0x4F58F660>("\xB2\xAC\xA7\x9B\xAA\xA8\xAB\xB1\x96\xBA\xA7\xAD\xB4\xAB\xBD" + 0x4F58F660).s), g_AimEspFixCfg.SndOnlyPlayer);

	return false;
}

int Parse_Config(const char *path)
{
	FILE *fp;
	fp = fopen(path,"rt");

	if(!fp) {
		return FALSE;
	}

	int cline = 0;
	char *pos;
	char buf[2048];

	while(!feof(fp))
	{
		if(!fgets(buf, sizeof(buf) - 1, fp)) {
			break;
		}

		if ((byte)buf[0] == 0xEFu && (byte)buf[1] == 0xBBu && (byte)buf[2] == 0xBFu)
			pos = &buf[3];
		else
			pos = buf;

		cline++;

		char* LineBuf = trimbuf(pos);

		if (LineBuf == nullptr
			|| LineBuf[0] == '\0'
			|| LineBuf[0] == '#'
			|| LineBuf[0] == ';'
			|| LineBuf[0] == '/'
			|| LineBuf[0] == '\\')
		{
			continue;
		}

		char* valSeparator = strchr(LineBuf, '=');

		if(valSeparator == nullptr) {
			continue;
		}

		*(valSeparator++) = 0;
		
		char* param = trimbuf(LineBuf);
		char* value = trimbuf(valSeparator);

		if(param == nullptr || value == nullptr) {
			continue;
		}

		if (!ParseCfgParam(param, value)) {
			AEF_PRINT(/*%s Config line parsing unknown parameter '%s' at line %d.*/XorStr<0x27, 58, 0x2C45B5CB>("\x02\x5B\x09\x69\x44\x42\x4B\x47\x48\x10\x5D\x5B\x5D\x51\x15\x46\x56\x4A\x4A\x53\x55\x5B\x1D\x4B\x51\x2B\x2F\x2D\x34\x2A\x65\x36\x26\x3A\x28\x27\x2E\x38\x28\x3C\x6F\x77\x74\x21\x74\x74\x34\x22\x77\x34\x30\x34\x3E\x7C\x78\x3A\x71" + 0x2C45B5CB).s, LOG_PREFIX, param, cline);
		}
	}

	return (fclose(fp) != EOF);
}

int Load_Config()
{
	char *pos;
	char mConfigPath[256];

	strcpy(mConfigPath, GET_PLUGIN_PATH(PLID));
	pos = strrchr(mConfigPath, '/');

	if(pos == nullptr || *pos == '\0') {
		return FALSE;
	}

	*(pos + 1) = '\0';

	char mPluginPath[256];

	strncpy(mPluginPath, mConfigPath, sizeof(mPluginPath) - 1);
	strcat(mConfigPath, CONFIG_FILE);

	g_AimEspFixCfg.AimFFA = 0;
	g_AimEspFixCfg.AimTabFix = 1;
	g_AimEspFixCfg.AimTabFlashedFix = 1;
	g_AimEspFixCfg.SndActive = 1;
	g_AimEspFixCfg.SndCount = 4;
	g_AimEspFixCfg.SndScalez = 4;
	g_AimEspFixCfg.SndRadius = 300;
	g_AimEspFixCfg.SndVolume = 4;
	g_AimEspFixCfg.SndOnlyPlayer = 0;

	return Parse_Config(mConfigPath);
}
