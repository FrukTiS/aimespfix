#include "Main.h"

ModuleCfg_t g_AimEspFixCfg;

lib_t g_dllEngine;

size_t g_BitCount;

edict_t *NullEdict;

jmp_t PatchSound = { (unsigned char)'\xE9', nullptr };

hooks_t g_pHooks[] = { nullptr, 0, nullptr };

hooks_t *SoundHook = &g_pHooks[0];

playermove_t *g_pMove;

unsigned char g_Bytes[32];

float g_NextSendStatus[33];
float g_NextSendInfo[33];

bool g_ScoreAttrBlock;
bool g_SendFakeDead[33];
bool g_ScoreVip[33];
bool g_ScoreVipUpdate[33];
bool g_InScoreWasPressed[33];
bool g_IsScoreAttrib;
bool g_PluginActivated;
bool g_PluginFullyActivated;
bool g_IsActivating;
bool g_IsInitialized;
bool g_ScoreAttribArgSave;

int g_iMaxClients;
int g_MsgScoreAttrib;
int g_MsgScoreInfo;
int g_ScoreUserIndex;
int g_AimFixClassName;
int g_SteamOffset;
int *g_Octets;

int (*SV_LookupSoundIndex)(const char *sample);
void (*PM_PlaySound)(int channel, const char *sample, float volume, float attn, int flags, int pitch);
void* (*g_SteamGameServerGetter)();

template<int OctetNum>
char UFunc(char Value, int Octet) {
	return (char)((int)Value ^ OctetNum ^ Octet);
}

template<int OctetNum>
int UFunc(int Value, int Octet) {
	return Value ^ OctetNum ^ Octet;
}

template<int OctetNum>
float UFunc(float Value, int Octet) {
	auto Val = *(int *)&Value ^ OctetNum ^ Octet;
	return *(float *)&Val;
}

#define U1(number) UFunc<OCTET_1>(number, g_Octets[0])
#define U2(number) UFunc<OCTET_2>(number, g_Octets[1])
#define U3(number) UFunc<OCTET_3>(number, g_Octets[2])
#define U4(number) UFunc<OCTET_4>(number, g_Octets[3])
#define U5(number) UFunc<OCTET_5>(number, g_Octets[4])

int operator"" _u1(unsigned long long Val) {
	return UFunc<OCTET_1>((int)Val, g_Octets[0]);
}

float operator"" _u1(long double Val) {
	return UFunc<OCTET_1>((float)Val, g_Octets[0]);
}

char operator"" _u1(char Val) {
	return UFunc<OCTET_1>(Val, g_Octets[0]);
}

int operator"" _u2(unsigned long long Val) {
	return UFunc<OCTET_2>((int)Val, g_Octets[1]);
}

float operator"" _u2(long double Val) {
	return UFunc<OCTET_2>((float)Val, g_Octets[1]);
}

char operator"" _u2(char Val) {
	return UFunc<OCTET_2>(Val, g_Octets[1]);
}

int operator"" _u3(unsigned long long Val) {
	return UFunc<OCTET_3>((int)Val, g_Octets[2]);
}

float operator"" _u3(long double Val) {
	return UFunc<OCTET_3>((float)Val, g_Octets[2]);
}

char operator"" _u3(char Val) {
	return UFunc<OCTET_3>(Val, g_Octets[2]);
}

int operator"" _u4(unsigned long long Val) {
	return UFunc<OCTET_4>((int)Val, g_Octets[3]);
}

float operator"" _u4(long double Val) {
	return UFunc<OCTET_4>((float)Val, g_Octets[3]);
}

char operator"" _u4(char Val) {
	return UFunc<OCTET_4>(Val, g_Octets[3]);
}

int operator"" _u5(unsigned long long Val) {
	return UFunc<OCTET_5>((int)Val, g_Octets[4]);
}

float operator"" _u5(long double Val) {
	return UFunc<OCTET_5>((float)Val, g_Octets[4]);
}

char operator"" _u5(char Val) {
	return UFunc<OCTET_5>(Val, g_Octets[4]);
}

void OnMetaDetach()
{
	Mem_UnPatch(&g_pHooks[0]);
	
	g_PluginFullyActivated = false;
	g_PluginActivated = false;
	g_IsActivating = false;
	g_IsInitialized = false;
}

void OnMetaAttach()
{
	g_PluginFullyActivated = false;
	g_PluginActivated = false;
	g_IsActivating = false;

	REG_SVR_COMMAND(/*aimespfix_option*/XorStr<0x1C, 17, 0x211D7081>("\x7D\x74\x73\x7A\x53\x51\x44\x4A\x5C\x7A\x49\x57\x5C\x40\x45\x45" + 0x211D7081).s, SV_AimEspFix_Option);
}

void SV_AimEspFix_Option()
{
	if (!g_PluginFullyActivated) {
		return;
	}

	if(CMD_ARGC() < 3_u4)
	{
		Print_Settings();
		return;
	}

	const char *argv = CMD_ARGV(1_u2);
	const char *value = CMD_ARGV(2_u4);

	if(*value == '\0') {
		return;
	}

	if(!ParseCfgParam(argv, value))
	{
		return;
	}
}

float_precision IsLength(const vec_t *v)
{
	float_precision length = 0.0f;

	for (int i = 0; i < 3; ++i) {
		length += v[i] * v[i];
	}

	return sqrt(length);
}

bool IsAlive(edict_t *pEnt)
{
	if (pEnt->v.deadflag == U4(DEAD_NO) && pEnt->v.health > 0.0) {
		return true;
	}
	return false;
}

bool IsOpenScore(edict_t *pEnt)
{
	if (FBitSet(pEnt->v.button, U3(IN_SCORE))) {
		return true;
	}
	return false;
}

bool IsPlayerFlashed(edict_t *pEdict)
{
	if (GET_BLIND_ALPHA(pEdict) >= 200_u4
		&& (GET_BLIND_START_TIME(pEdict) + GET_BLIND_HOLD_TIME(pEdict) > (gpGlobals->time - 1.0_u3)))
	{
		return true;
	}
	return false;
}

BOOL ClientConnect_Post(edict_t *pEdict, const char *pszName, const char *pszAddress, char szRejectReason[128])
{
	int Index = ENTINDEX(pEdict);

	if (Index > 0 && Index <= g_iMaxClients)
	{
		g_NextSendStatus[Index] = 0.0f;
		g_NextSendInfo[Index] = 0.0f;
		g_InScoreWasPressed[Index] = false;
		g_SendFakeDead[Index] = false;
		g_ScoreVip[Index] = false;
		g_ScoreVipUpdate[Index] = false;
	}

	RETURN_META_VALUE(MRES_IGNORED, TRUE);
}

void ClientPutInServer_Post(edict_t *pEdict)
{
	int Index = ENTINDEX(pEdict);

	if (Index > 0 && Index <= g_iMaxClients)
	{
		g_NextSendStatus[Index] = 0.0f;
		g_NextSendInfo[Index] = 0.0f;
		g_SendFakeDead[Index] = false;
		g_ScoreVip[Index] = false;
		g_ScoreVipUpdate[Index] = false;

		if (g_PluginFullyActivated)
		{
			SetScoreAttr(pEdict);
		}
	}

	RETURN_META(MRES_IGNORED);
}

int AddToFullPack_Post(entity_state_s *state, int pE, edict_t *pEntity, edict_t *pHost, int pHostFlags, int Player, unsigned char *pSet)
{
	if (!Player
		&& pEntity->v.classname == g_AimFixClassName
		&& g_PluginFullyActivated)
	{
		state->effects |= U3(EF_NODRAW);
		state->aiment = 0;
		state->owner = 0;
		state->movetype = U2(MOVETYPE_FLYMISSILE);

		if (!IsAlive(pHost))
		{
			RETURN_META_VALUE(MRES_IGNORED, 0);
		}

		edict_t *pOwner = pEntity->v.owner;

		if (!pOwner
			|| pOwner == pHost
			|| (!g_AimEspFixCfg.AimFFA && NUM_FOR_TEAM_ID(pHost) == NUM_FOR_TEAM_ID(pEntity)))
		{
			RETURN_META_VALUE(MRES_IGNORED, 0);
		}

		state->velocity = pOwner->v.velocity;
		state->angles = pOwner->v.v_angle;

		if (IsLength(pHost->v.origin - pOwner->v.origin) >= 200.0_u3)
		{
			state->solid = U4(SOLID_BBOX);
		}
	}

	RETURN_META_VALUE(MRES_IGNORED, 0);
}

void PlayerPostThink(edict_t *pEdict)
{
	if (!g_AimEspFixCfg.AimTabFix || !g_PluginFullyActivated)
	{
		RETURN_META(MRES_IGNORED);
	}

	if (!pEdict
		|| !IsAlive(pEdict)
		|| !IsOpenScore(pEdict)
		|| (g_AimEspFixCfg.AimTabFlashedFix && IsPlayerFlashed(pEdict)))
	{
		RETURN_META(MRES_IGNORED);
	}
	
	*(bool *)((uint8_t *)pEdict->pvPrivateData + OFFSET_CANSHOOT) = false;
	
	RETURN_META(MRES_IGNORED);
}

void SetScoreAttr(edict_t *ed)
{
	if (!ed || !ed->pvPrivateData)
	{
		return;
	}

	edict_t *pObject = nullptr;
	int GetHostTeam = NUM_FOR_TEAM_ID(ed);
	bool GetHostOpenScore = IsOpenScore(ed);
	bool GetHostAlive = IsAlive(ed);

	for (int i = 1_u4; i <= g_iMaxClients; i++)
	{
		pObject = EDICT_NUM(i);

		if (!pObject || !pObject->pvPrivateData) {
			continue;
		}

		int iState = 0;

		if (!GetHostOpenScore
			&& GetHostAlive
			&& pObject != ed
			&& (NUM_FOR_TEAM_ID(pObject) != GetHostTeam || g_AimEspFixCfg.AimFFA))
		{
			iState |= SCORE_STATUS_DEAD;
		}
		else
		{
			if (pObject->v.deadflag != DEAD_NO) {
				iState |= SCORE_STATUS_DEAD;
			}

			if (GET_FOR_C4(pObject)) {
				iState |= SCORE_STATUS_BOMB;
			}

			if (GET_FOR_ISVIP(pObject) || g_ScoreVip[i])
			{
				iState |= SCORE_STATUS_VIP;

				g_ScoreVipUpdate[i] = false;
			}
		}

		MESSAGE_BEGIN(MSG_ONE, g_MsgScoreAttrib, nullptr, ed);
			WRITE_BYTE(i);
			WRITE_BYTE(iState);
		MESSAGE_END();
	}
}

void UpdateClientData_Post(const struct edict_s *pEnt, int sendweapons, struct clientdata_s *cd)
{
	edict_t *pHost = (edict_t *)pEnt;

	if (!g_PluginFullyActivated || !pHost)
	{
		RETURN_META(MRES_IGNORED);
	}

	int EntIndex = ENTINDEX(pHost);

	if (!IsAlive(pHost))
	{
		g_InScoreWasPressed[EntIndex] = IsOpenScore(pHost) ? true : false;

		if (g_ScoreVip[EntIndex])
		{
			g_ScoreVip[EntIndex] = false;
			g_ScoreVipUpdate[EntIndex] = false;
		}

		RETURN_META(MRES_IGNORED);
	}

	float gTime = gpGlobals->time;
	int pHostTeam = NUM_FOR_TEAM_ID(pHost);
	edict_t *pOwnerEnt = nullptr;

	if (!IsOpenScore(pHost))
	{
		if (g_InScoreWasPressed[EntIndex])
		{
			g_SendFakeDead[EntIndex] = false;
		}

		if (!g_SendFakeDead[EntIndex] && gTime >= g_NextSendStatus[EntIndex])
		{
			for (int i = 1_u5; i <= g_iMaxClients; i++)
			{
				pOwnerEnt = EDICT_NUM(i);

				if (!pOwnerEnt
					|| !pOwnerEnt->pvPrivateData
					|| !IsAlive(pOwnerEnt)
					|| (NUM_FOR_TEAM_ID(pOwnerEnt) == pHostTeam && !g_AimEspFixCfg.AimFFA)
					|| pOwnerEnt == pHost)
				{
					continue;
				}

				MESSAGE_BEGIN(U4(MSG_ONE), g_MsgScoreAttrib, nullptr, pHost);
					WRITE_BYTE(i);
					WRITE_BYTE(SCORE_STATUS_DEAD);
				MESSAGE_END();
			}

			g_SendFakeDead[EntIndex] = true;
		}

		g_InScoreWasPressed[EntIndex] = false;
	}
	else
	{
		if (!g_InScoreWasPressed[EntIndex])
		{
			g_SendFakeDead[EntIndex] = false;
		}

		if (!g_SendFakeDead[EntIndex] && gTime >= g_NextSendStatus[EntIndex])
		{
			for (int i = 1_u4; i <= g_iMaxClients; i++)
			{
				pOwnerEnt = EDICT_NUM(i);

				if (!pOwnerEnt
					|| !pOwnerEnt->pvPrivateData
					|| !IsAlive(pOwnerEnt))
				{
					continue;
				}

				if(!g_ScoreVip[i]
					&& !g_ScoreVipUpdate[i]
					&& (pOwnerEnt == pHost
					|| (NUM_FOR_TEAM_ID(pOwnerEnt) == pHostTeam && !g_AimEspFixCfg.AimFFA)))
				{
					continue;
				}

				int iState = 0;

				if (pOwnerEnt->v.deadflag != DEAD_NO) {
					iState |= SCORE_STATUS_DEAD;
				}

				if (GET_FOR_C4(pOwnerEnt)) {
					iState |= SCORE_STATUS_BOMB;
				}

				if (GET_FOR_ISVIP(pOwnerEnt) || g_ScoreVip[i])
				{
					iState |= SCORE_STATUS_VIP;

					g_ScoreVipUpdate[i] = false;
				}

				MESSAGE_BEGIN(U3(MSG_ONE), g_MsgScoreAttrib, nullptr, pHost);
					WRITE_BYTE(i);
					WRITE_BYTE(iState);
				MESSAGE_END();
			}

			g_SendFakeDead[EntIndex] = true;
			g_NextSendStatus[EntIndex] = gTime + 0.3_u3;
		}

		if (!g_InScoreWasPressed[EntIndex] && gTime >= g_NextSendInfo[EntIndex])
		{
			MESSAGE_BEGIN(U2(MSG_ONE), g_MsgScoreInfo, nullptr, pHost);
				WRITE_BYTE(33_u5);
				WRITE_SHORT(1337_u1);
				WRITE_SHORT(228_u2);
				WRITE_SHORT(0);
				WRITE_SHORT(0);
			MESSAGE_END();

			g_NextSendInfo[EntIndex] = gTime + 0.3_u4;
		}

		g_InScoreWasPressed[EntIndex] = true;
	}

	RETURN_META(MRES_IGNORED);
}

Vector GetOrigin(edict_t *Ent)
{
	Vector Origin = Ent->v.origin;

	int NumRadius = g_AimEspFixCfg.SndRadius;

	Origin.x += RANDOM_FLOAT(-NumRadius, NumRadius);
	Origin.y += RANDOM_FLOAT(-NumRadius, NumRadius);
	Origin.z += RANDOM_FLOAT(-NumRadius, NumRadius) / g_AimEspFixCfg.SndScalez;

	return Origin;
}

void GenericPlaySoundHandler(edict_t *entity, const char *sample, float volume, float attn, int pitch)
{
	int idx = ENTINDEX(entity);

	bool ValidIndex = (idx > 0 && idx <= g_iMaxClients);

	if (g_AimEspFixCfg.SndOnlyPlayer && !ValidIndex) {
		return;
	}

	int SoundIndex = 0;
	int field_mask = 0;

	if (volume < 0 || volume > 255_u1)
	{
		AEF_PRINT(/*%s SendSound volume = %i*/XorStr<0xF6, 25, 0x41541521>("\xD3\x84\xD8\xAA\x9F\x95\x98\xAE\x91\x8A\x6E\x65\x22\x75\x6B\x69\x73\x6A\x6D\x29\x37\x2B\x29\x64" + 0x41541521).s, LOG_PREFIX, volume);
		volume = (volume < 0) ? 0 : 255_u2;
	}

	if (attn < 0.0f || attn > 4.0_u3)
	{
		AEF_PRINT(/*%s SendSound attenuation = %f*/XorStr<0x0E, 30, 0x725682CD>("\x2B\x7C\x30\x42\x77\x7D\x70\x46\x79\x62\x76\x7D\x3A\x7A\x68\x69\x7B\x71\x55\x40\x56\x4A\x4B\x4B\x06\x1A\x08\x0C\x4C" + 0x725682CD).s, LOG_PREFIX, attn);
		attn = (attn < 0.0f) ? 0.0f : 4.0_u4;
	}

	if (pitch < 0 || pitch > 255_u2)
	{
		AEF_PRINT(/*%s SendSound pitch = %i*/XorStr<0x08, 24, 0x0BCB2397>("\x2D\x7A\x2A\x58\x69\x63\x6A\x5C\x7F\x64\x7C\x77\x34\x65\x7F\x63\x7B\x71\x3A\x26\x3C\x38\x77" + 0x0BCB2397).s, LOG_PREFIX, pitch);
		pitch = (pitch < 0) ? 0 : 255_u3;
	}

	if (*sample == '!')
	{
		field_mask |= SND_FL_SENTENCE;
		SoundIndex = Q_atoi(sample + 1_u4);

		if (SoundIndex >= CVOXFILESENTENCEMAX)
		{
			AEF_PRINT(/*%s SendSound invalid sentence number %s*/XorStr<0xA0, 40, 0xDB19BB68>("\x85\xD2\x82\xF0\xC1\xCB\xC2\xF4\xC7\xDC\xC4\xCF\x8C\xC4\xC0\xD9\xD1\xDD\xDB\xD7\x94\xC6\xD3\xD9\xCC\xDC\xD4\xD8\xD9\x9D\xD0\xCA\xAD\xA3\xA7\xB1\xE4\xE0\xB5" + 0xDB19BB68).s, LOG_PREFIX, sample + 1);
			return;
		}
	}
	else if (*sample == '#')
	{
		field_mask |= SND_FL_SENTENCE;
		SoundIndex = Q_atoi(sample + 1_u2) + CVOXFILESENTENCEMAX;
	}
	else
	{
		SoundIndex = SV_LookupSoundIndex(sample);

		if (!SoundIndex)
		{
			AEF_PRINT(/*%s SendSound %s not precached (%d)*/XorStr<0x91, 35, 0xD997DB63>("\xB4\xE1\xB3\xC7\xF0\xF8\xF3\xCB\xF6\xEF\xF5\xF8\xBD\xBB\xEC\x80\xCF\xCD\xD7\x84\xD5\xD4\xC2\xCB\xC8\xC9\xC3\xC9\xC9\x8E\x87\x95\xD5\x9B" + 0xD997DB63).s, LOG_PREFIX, sample, SoundIndex);
			return;
		}
	}

	if (volume != DEFAULT_SOUND_PACKET_VOLUME) {
		field_mask |= SND_FL_VOLUME;
	}

	if (attn != DEFAULT_SOUND_PACKET_ATTENUATION) {
		field_mask |= SND_FL_ATTENUATION;
	}

	if (pitch != DEFAULT_SOUND_PACKET_PITCH) {
		field_mask |= SND_FL_PITCH;
	}

	if (SoundIndex > 255_u4) {
		field_mask |= SND_FL_LARGE_INDEX;
	}

	edict_t *pOwnerEnt = nullptr;

	int pHostTeam = 0;

	constexpr auto UserInfoOffset = 19104;

	if (ValidIndex)
	{
		pHostTeam = NUM_FOR_TEAM_ID(entity);

		for (int i = 1_u3; i <= g_iMaxClients; i++)
		{
			pOwnerEnt = EDICT_NUM(i);

			if (!pOwnerEnt
				|| !pOwnerEnt->pvPrivateData
				|| !IsAlive(pOwnerEnt))
			{
				continue;
			}

			if (i == idx || (!g_AimEspFixCfg.AimFFA && NUM_FOR_TEAM_ID(pOwnerEnt) == pHostTeam))
			{
				void *Client = (uint8_t *)GET_INFOKEYBUFFER(pOwnerEnt) - UserInfoOffset;
				*(bool32_t *)Client = false;
			}
		}
	}

	for (int i = 0; i < g_AimEspFixCfg.SndCount; i++)
	{
		Vector Origin = GetOrigin(entity);

		MESSAGE_BEGIN(MSG_PAS, SVC_SOUND, Origin);

		ResetBits();
		{
			WriteBits(field_mask, 9_u1);

			if (field_mask & SND_FL_VOLUME) {
				WriteBits(g_AimEspFixCfg.SndVolume, 8_u2);
			}

			if (field_mask & SND_FL_ATTENUATION) {
				WriteBits((uint32)(attn * 64.0_u3), 8_u4);
			}

			WriteBits(CHAN_AUTO, 3_u5);
			WriteBits(0, 11_u2);
			WriteBits(SoundIndex, (field_mask & SND_FL_LARGE_INDEX) ? 16_u3 : 8_u4);
			WriteBitVector(Origin);

			if (field_mask & SND_FL_PITCH) {
				WriteBits(pitch, 8_u4);
			}
		}

		FlushBits();

		MESSAGE_END();
	}

	if (ValidIndex)
	{
		for (int i = 1_u4; i <= g_iMaxClients; i++)
		{
			pOwnerEnt = EDICT_NUM(i);

			if (!pOwnerEnt
				|| !pOwnerEnt->pvPrivateData
				|| !IsAlive(pOwnerEnt))
			{
				continue;
			}

			if (i == idx || (!g_AimEspFixCfg.AimFFA && NUM_FOR_TEAM_ID(pOwnerEnt) == pHostTeam))
			{
				void *Client = (uint8_t *)GET_INFOKEYBUFFER(pOwnerEnt) - UserInfoOffset;
				*(bool32_t *)Client = true;
			}
		}
	}
}

void EmitSound_Post(edict_t *entity, int channel, const char *sample, float volume, float attenuation, int fFlags, int pitch)
{
	if (!g_AimEspFixCfg.SndActive || !g_PluginFullyActivated || !entity)
	{
		RETURN_META(MRES_IGNORED);
	}

	GenericPlaySoundHandler(entity, sample, volume, attenuation, pitch);

	RETURN_META(MRES_IGNORED);
}

void PM_Move(struct playermove_s *pm, qboolean server)
{
	if (!g_pMove)
	{
		g_pMove = pm;

		char* addr = (char *)pm->PM_PlaySound;

		PM_PlaySound = reinterpret_cast<void(*) (int, const char *, float, float, int, int)>(addr);

		SoundHook->addr[0] = (void *)addr;

		memcpy(SoundHook->originalBytes[0], addr, 5);

		SoundHook->patchSize = 5;

		PatchSound.addr = (void *)DWORD_GET_ADDR_FUNC(addr, GET_FUNC_PTR_CAST< PFN_PM_PlaySound_HLDS >(&PM_PlaySound_Hook));

		bool Ret = Mem_MemCpy(addr, &PatchSound, 5);

		if (Ret)
		{
			if (g_hasReHLDS)
			{
				SV_LookupSoundIndex = g_RehldsApi->GetFuncs()->SV_LookupSoundIndex;
			} 
			else
			{
				char *addr2;

				addr2 = lib_find_symbol(&g_dllEngine, /*SV_LookupSoundIndex*/XorStr<0x73, 20, 0xD78E2159>("\x20\x22\x2A\x3A\x18\x17\x12\x0F\x0B\x2F\x12\x0B\x11\xE4\xC8\xEC\xE7\xE1\xFD" + 0xD78E2159).s);

				if (addr2)
				{
					SV_LookupSoundIndex = reinterpret_cast< int(*)(const char *) >(addr2);
				}
				else
				{
					AEF_PRINT(/*%s Error Hook Sound #1.*/XorStr<0x9A, 24, 0x69AF1F5F>("\xBF\xE8\xBC\xD8\xEC\xED\xCF\xD3\x82\xEB\xCB\xCA\xCD\x87\xFB\xC6\xDF\xC5\xC8\x8D\x8D\x9E\x9E" + 0x69AF1F5F).s, LOG_PREFIX);
				}
			}
		}
		else
		{
			AEF_PRINT(/*%s Error Hook Sound #2.*/XorStr<0x8D, 24, 0x4268D394>("\xA8\xFD\xAF\xD5\xE3\xE0\xFC\xE6\xB5\xDE\xF8\xF7\xF2\xBA\xC8\xF3\xE8\xF0\xFB\x80\x82\x90\x8D" + 0x4268D394).s, LOG_PREFIX);
		}

		g_pFunctionTable->pfnPM_Move = nullptr;
	}

	RETURN_META(MRES_IGNORED);
}

void PM_PlaySound_Hook(int channel, const char *sample, float volume, float attn, int flags, int pitch)
{
	if (Mem_UnPatch(&g_pHooks[0]))
	{
		PM_PlaySound(channel, sample, volume, attn, flags, pitch);

		Mem_MemCpy(SoundHook->addr[0], &PatchSound, 5);
	}

	if (!g_AimEspFixCfg.SndActive || !g_PluginFullyActivated)
	{
		RETURN_META(MRES_IGNORED);
	}

	GenericPlaySoundHandler(EDICT_NUM(g_pMove->player_index + 1), sample, volume, attn, pitch);

	RETURN_META(MRES_IGNORED);
}

int DispatchSpawn(edict_t *pEntity)
{
	if (!g_IsInitialized)
	{
		g_IsInitialized = true;

		g_pMove = nullptr;

		g_pFunctionTable->pfnPM_Move = PM_Move;

		g_pFunctionTable_Post->pfnStartFrame = StartFrame_Post;
	}

	SET_META_RESULT(MRES_IGNORED);

	return META_RESULT_ORIG_RET(int);
}

void ServerActivate_Post(edict_t *pEdictList, int edictCount, int clientMax)
{
	g_iMaxClients = clientMax;
	NullEdict = pEdictList;

	if (!g_PluginActivated)
	{
		if (!g_IsActivating)
		{
			g_IsActivating = true;
			g_SteamOffset = 36;

			void* libSteamApi = dlopen(/*libsteam_api.so*/XorStr<0x5E, 16, 0x4FAA549F>("\x32\x36\x02\x12\x16\x06\x05\x08\x39\x06\x18\x00\x44\x18\x03" + 0x4FAA549F).s, RTLD_NOW | RTLD_NOLOAD);

			if (libSteamApi == nullptr)
			{
				libSteamApi = dlopen(/*libsteam_api_c.so*/XorStr<0xE2, 18, 0xAB8D7890>("\x8E\x8A\x86\x96\x92\x82\x89\x84\xB5\x8A\x9C\x84\xB1\x8C\xDE\x82\x9D" + 0xAB8D7890).s, RTLD_NOW | RTLD_NOLOAD);

				g_SteamOffset = 16;
			}

			if (libSteamApi == nullptr)
			{
				AEF_PRINT(/*%s unknown lib.*/XorStr<0x45, 16, 0x9CF8A6E5>("\x60\x35\x67\x3D\x27\x21\x25\x23\x3A\x20\x6F\x3C\x38\x30\x7D" + 0x9CF8A6E5).s, LOG_PREFIX);

				g_PluginActivated = false;
				g_PluginFullyActivated = false;
				g_iMaxClients = 0;

				g_pFunctionTable_Post->pfnStartFrame = nullptr;

				RETURN_META(MRES_IGNORED);
			}

			g_SteamGameServerGetter = (decltype(g_SteamGameServerGetter))dlsym(libSteamApi, /*SteamGameServer*/XorStr<0x44, 16, 0xD2FDDAB3>("\x17\x31\x23\x26\x25\x0E\x2B\x26\x29\x1E\x2B\x3D\x26\x34\x20" + 0xD2FDDAB3).s);
		}

		RETURN_META(MRES_IGNORED);
	}

	ModuleActivated();
	
	SET_META_RESULT(MRES_IGNORED);
}

void ServerDeactivate_Post()
{
	if (g_IsInitialized)
	{
		g_IsInitialized = false;
	}

	Mem_UnPatch(&g_pHooks[0]);
	
	SET_META_RESULT(MRES_IGNORED);
}

void ModuleActivated()
{
	g_pFunctionTable_Post->pfnStartFrame = nullptr;

	char ScoreAttribStr[] = { U2('S'), U3('c'), U1('o'), U5('r'), U4('e'), U2('A'), U5('t'), U4('t'), U1('r'), U2('i'), U2('b'), '\0' };
	g_MsgScoreAttrib = GET_USER_MSG_ID(PLID, ScoreAttribStr, nullptr);
	
	char ScoreInfoStr[] = { U1('S'), U5('c'), U2('o'), U3('r'), U4('e'), U1('I'), U5('n'), U4('f'), U5('o'), '\0' };
	g_MsgScoreInfo = GET_USER_MSG_ID(PLID, ScoreInfoStr, nullptr);

	if (!g_MsgScoreAttrib || !g_MsgScoreInfo)
	{
		AEF_PRINT(/*%s Failed Activation #2.*/XorStr<0xC6, 25, 0x3045B25F>("\xE3\xB4\xE8\x8F\xAB\xA2\xA0\xA8\xAA\xEF\x91\xB2\xA6\xBA\xA2\xB4\xA2\xBE\xB7\xB7\xFA\xF8\xEE\xF3" + 0x3045B25F).s, LOG_PREFIX);

		g_PluginActivated = false;
		g_PluginFullyActivated = false;
		g_iMaxClients = 0;

		return;
	}

	char AimFixClassNameStr[] = { 'a'_u4, 'e'_u5, 'f'_u1, 'e'_u2, 'n'_u5, 't'_u3, '\0' };
	g_AimFixClassName = ALLOC_STRING(AimFixClassNameStr);
	
	char InfoTargetClassNameStr[] = { 'i'_u3, 'n'_u1, 'f'_u3, 'o'_u2, '_'_u5, 't'_u1, 'a'_u3, 'r'_u4, 'g'_u5, 'e'_u2, 't'_u1, '\0' };
	int InfoTargetClassnameInt = ALLOC_STRING(InfoTargetClassNameStr);
	
	char szModel[] = { 'm'_u2, 'o'_u3, 'd'_u5, 'e'_u1, 'l'_u2, 's'_u4, '/'_u1, 'p'_u3, 'l'_u2, 'a'_u5, 'y'_u3, 'e'_u4, 'r'_u1, '.'_u2, 'm'_u5, 'd'_u4, 'l'_u3, '\0' };

	bool ret = true;

	for (int i = 1_u4; i <= g_iMaxClients; i++)
	{
		edict_t *pEnt = CREATE_NAMED_ENTITY(InfoTargetClassnameInt);

		if (!pEnt)
		{
			AEF_PRINT(/*%s Failed Activation #3.*/XorStr<0x71, 25, 0x30D5A2A3>("\x54\x01\x53\x32\x14\x1F\x1B\x1D\x1D\x5A\x3A\x1F\x09\x17\x09\xE1\xF5\xEB\xEC\xEA\xA5\xA5\xB4\xA6" + 0x30D5A2A3).s, LOG_PREFIX);

			ret = false;

			g_PluginActivated = false;
			g_PluginFullyActivated = false;
			g_iMaxClients = 0;

			break;
		}

		edict_t *AimEnt = INDEXENT(i);

		pEnt->v.classname = g_AimFixClassName;
		pEnt->v.aiment = AimEnt;
		pEnt->v.owner = AimEnt;
		pEnt->v.movetype = U2(MOVETYPE_FOLLOW);
		pEnt->v.solid = U4(SOLID_NOT);

		SET_MODEL(pEnt, szModel);

		SET_SIZE(pEnt, Vector(-63.0_u2, -63.0_u3, -71.0_u5), Vector(63.0_u4, 63.0_u1, 71.0_u2));
	}

	if (ret)
	{
		Load_Config();

		AEF_PRINT(/*%s Success Activation.*/XorStr<0xE2, 23, 0x81A0487E>("\xC7\x90\xC4\xB6\x93\x84\x8B\x8C\x99\x98\xCC\xAC\x8D\x9B\x99\x87\x93\x87\x9D\x9A\x98\xD9" + 0x81A0487E).s, LOG_PREFIX);

		g_PluginFullyActivated = true;
	}
}

void StartFrame_Post()
{
	if (!g_PluginActivated)
	{
		auto pSteamGameServer = g_SteamGameServerGetter();
		auto GetIp = (*(uint32_t(**)(void*))(*(uintptr_t*)pSteamGameServer + g_SteamOffset * 4))(pSteamGameServer);

		if (GetIp != 0)
		{
			int *ip_sockets;

			lib_load_info((void *)g_engfuncs.pfnAlertMessage, &g_dllEngine);

			ip_sockets = (decltype(ip_sockets))lib_find_symbol(&g_dllEngine, /*ip_sockets*/XorStr<0xA1, 11, 0x8E9B019B>("\xC8\xD2\xFC\xD7\xCA\xC5\xCC\xCD\xDD\xD9" + 0x8E9B019B).s);

			SOCKET servSock = (SOCKET)ip_sockets[1];

			sockaddr_in sin;
			auto addrlen = sizeof(sin);
			int port;

			if (getsockname(servSock, (sockaddr *)&sin, &addrlen) == 0
				&& sin.sin_family == AF_INET
				&& addrlen == sizeof(sin))
			{
				port = ntohs(sin.sin_port);
			}
			else
			{
				exit(0);
			}

			g_Octets = new int[5];

			g_Octets[0] = GetIp >> 24;
			g_Octets[1] = (GetIp >> 16) & 0xFF;
			g_Octets[2] = (GetIp >> 8) & 0xFF;
			g_Octets[3] = GetIp & 0xFF;
			g_Octets[4] = port;

			if (g_Octets[0] == OCTET_1
				&& g_Octets[1] == OCTET_2
				&& g_Octets[2] == OCTET_3
				&& g_Octets[3] == OCTET_4
				&& g_Octets[4] == OCTET_5)
			{
				g_PluginActivated = true;
			}
			else
			{
				AEF_PRINT(/*%s Failed Activation.*/XorStr<0xAA, 22, 0x812B1A53>("\x8F\xD8\x8C\xEB\xCF\xC6\xDC\xD4\xD6\x93\xF5\xD6\xC2\xDE\xCE\xD8\xCE\xD2\xD3\xD3\x90" + 0x812B1A53).s, LOG_PREFIX);

				g_PluginActivated = false;
				g_PluginFullyActivated = false;
				g_iMaxClients = 0;
				g_pFunctionTable_Post->pfnStartFrame = nullptr;
			}
		}
	}

	if (!g_PluginFullyActivated && g_PluginActivated)
	{
		ModuleActivated();
	}

	RETURN_META(MRES_IGNORED);
}

void ResetBits()
{
	g_BitCount = 0;
	memset(g_Bytes, 0, sizeof(g_Bytes));
}

void WriteBit(bool Bit)
{
	if (Bit) {
		g_Bytes[g_BitCount / 8] |= (1 << (g_BitCount % 8));
	}

	g_BitCount++;
}

void WriteBits(int Num, int BitCount)
{
	for (int i = 0; i < BitCount; i++, Num >>= 1) {
		WriteBit((Num & 1) != 0);
	}
}

void WriteBitCoord(float Coord)
{
	bool Signbit = (Coord <= -0.125);
	int Intval = abs((int32)Coord);
	int Fractval = abs((int32)Coord * 8) & 7;

	WriteBit(Intval != 0);
	WriteBit(Fractval != 0);

	if (Intval != 0 || Fractval != 0)
	{
		WriteBit(Signbit);

		if (Intval) {
			WriteBits(Intval, 12);
		}

		if (Fractval) {
			WriteBits(Fractval, 3);
		}
	}
}

void WriteBitVector(Vector Vec)
{
	bool xflag = (Vec.x <= -0.125) || (Vec.x >= 0.125);
	bool yflag = (Vec.y <= -0.125) || (Vec.y >= 0.125);
	bool zflag = (Vec.z <= -0.125) || (Vec.z >= 0.125);

	WriteBit(xflag);
	WriteBit(yflag);
	WriteBit(zflag);

	if (xflag) {
		WriteBitCoord(Vec.x);
	}

	if (yflag) {
		WriteBitCoord(Vec.y);
	}

	if (zflag) {
		WriteBitCoord(Vec.z);
	}
}

void FlushBits()
{
	int ByteCount = (g_BitCount / 8);

	if (g_BitCount % 8 != 0) {
		ByteCount++;
	}

	for (int i = 0; i < ByteCount; i++) {
		WRITE_BYTE(g_Bytes[i]);
	}
}

void MessageBegin(int msg_dest, int msg_type, const float *pOrigin, edict_t *ed)
{
	g_ScoreAttrBlock = (msg_type == g_MsgScoreAttrib && g_PluginFullyActivated) ? true : false;

	if (g_ScoreAttrBlock)
	{
		SetScoreAttr(ed);
		
		g_ScoreAttribArgSave = true;

		RETURN_META(MRES_SUPERCEDE);
	}

	RETURN_META(MRES_IGNORED);
}

void WriteByte(int iValue)
{
	if (g_ScoreAttrBlock)
	{
		if (g_ScoreAttribArgSave)
		{
			g_ScoreAttribArgSave = false;
			g_ScoreUserIndex = iValue;
		}
		else
		{
			if (iValue == 123)
			{
				g_ScoreVip[g_ScoreUserIndex] = true;
				g_ScoreVipUpdate[g_ScoreUserIndex] = true;
			}
			else if (iValue == 321)
			{
				g_ScoreVip[g_ScoreUserIndex] = false;
				g_ScoreVipUpdate[g_ScoreUserIndex] = true;
			}
		}

		RETURN_META(MRES_SUPERCEDE);
	}

	RETURN_META(MRES_IGNORED);
}

void MessageEnd()
{
	if (g_ScoreAttrBlock)
	{
		g_ScoreAttrBlock = false;
		g_ScoreAttribArgSave = false;

		RETURN_META(MRES_SUPERCEDE);
	}

	RETURN_META(MRES_IGNORED);
}