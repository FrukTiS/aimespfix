#include "Memory.h"

bool Mem_MemCpy(void *addr, void *patch, int len)
{
	uint32_t size = sysconf(_SC_PAGESIZE);
	void *alignedAddress = Align(addr);

	if (Align(addr + len - 1) != alignedAddress)
	{
		size *= 2;
	}

	if (!mprotect(alignedAddress, size, (PROT_READ | PROT_WRITE | PROT_EXEC)))
	{
		memcpy(addr, patch, len);
		return !mprotect(alignedAddress, size, (PROT_READ | PROT_EXEC));
	}

	return false;
}

bool Mem_UnPatch(hooks_t *a)
{
	if (!a || !a->addr[0]) {
		return false;
	}

	for (int i = 0; i < ARRAYSIZE(a->addr); i++)
	{
		if (!a->addr[i]) {
			break;
		}

		Mem_MemCpy(a->addr[i], a->originalBytes[i], a->patchSize);
	}

	return true;
}

ElfW(Addr) dlsize(void *base)
{
	int i;
	ElfW(Ehdr) *ehdr;
	ElfW(Phdr) *phdr;
	ElfW(Addr) end = 0;

	ehdr = (ElfW(Ehdr)*)base;
	phdr = (ElfW(Phdr)*)((ElfW(Addr))ehdr + ehdr->e_phoff);

	for (i = 0; i < ehdr->e_phnum; ++i)
	{
		if (phdr[i].p_type == PT_LOAD) {
			end = phdr[i].p_vaddr + phdr[i].p_memsz;
		}
	}

	return end;
}

char *lib_find_symbol(lib_t *lib, const char *symbolName)
{
	int i;
	link_map *dlmap;
	struct stat dlstat;
	int dlfile;
	uintptr_t map_base;
	Elf32_Ehdr *file_hdr;
	Elf32_Shdr *sections, *shstrtab_hdr, *symtab_hdr, *strtab_hdr;
	Elf32_Sym *symtab;
	const char *shstrtab, *strtab;
	uint16 section_count;
	uint32 symbol_count;
	char *addr;
	int index = 0;

#if 0
	if (index == 0)
	{
		addr = (char *)dlsym((void *)lib->handle, symbolName);

		if (addr != NULL) {
			return addr;
		}
	}
#endif

	dlmap = (struct link_map *)lib->handle;
	symtab_hdr = NULL;
	strtab_hdr = NULL;

	dlfile = open(dlmap->l_name, O_RDONLY);

	if (dlfile == -1 || fstat(dlfile, &dlstat) == -1)
	{
		close(dlfile);
		return NULL;
	}

	file_hdr = (Elf32_Ehdr *)mmap(NULL, dlstat.st_size, PROT_READ, MAP_PRIVATE, dlfile, 0);
	map_base = (uintptr_t)file_hdr;
	close(dlfile);

	if (file_hdr == MAP_FAILED) {
		return NULL;
	}

	if (file_hdr->e_shoff == 0 || file_hdr->e_shstrndx == SHN_UNDEF)
	{
		munmap(file_hdr, dlstat.st_size);
		return NULL;
	}

	sections = (Elf32_Shdr *)(map_base + file_hdr->e_shoff);
	section_count = file_hdr->e_shnum;

	shstrtab_hdr = &sections[file_hdr->e_shstrndx];
	shstrtab = (const char *)(map_base + shstrtab_hdr->sh_offset);

	for (uint16 i = 0; i < section_count; i++)
	{
		Elf32_Shdr &hdr = sections[i];
		const char *section_name = shstrtab + hdr.sh_name;

		if (strcmp(section_name, /*.symtab*/XorStr<0x9E, 8, 0x2D3ED131>("\xB0\xEC\xD9\xCC\xD6\xC2\xC6" + 0x2D3ED131).s) == 0)
		{
			symtab_hdr = &hdr;
		}
		else if (strcmp(section_name, /*.strtab*/XorStr<0x70, 8, 0x962F6DE4>("\x5E\x02\x06\x01\x00\x14\x14" + 0x962F6DE4).s) == 0)
		{
			strtab_hdr = &hdr;
		}
	}

	if (symtab_hdr == NULL || strtab_hdr == NULL)
	{
		munmap(file_hdr, dlstat.st_size);
		return NULL;
	}

	symtab = (Elf32_Sym *)(map_base + symtab_hdr->sh_offset);
	strtab = (const char *)(map_base + strtab_hdr->sh_offset);
	symbol_count = symtab_hdr->sh_size / symtab_hdr->sh_entsize;

	int mangleNameLength;
	int mangleNameLastLength = 1024;

	if (index == 0) {
		index++;
	}

	int match = 1;

	for (uint32 i = 0; i < symbol_count; i++)
	{
		Elf32_Sym &sym = symtab[i];
		unsigned char sym_type = ELF32_ST_TYPE(sym.st_info);
		const char *sym_name = strtab + sym.st_name;

		if (sym.st_shndx == SHN_UNDEF || (sym_type != STT_FUNC && sym_type != STT_OBJECT)) {
			continue;
		}

		if (strcmp(sym_name, symbolName) == 0)
		{
			if (match == index)
			{
				addr = (char *)(dlmap->l_addr + sym.st_value);
				break;
			}
			else
			{
				match++;
			}
		}

		if (sym_name[0] == '_' && sym_name[1] == 'Z' && strstr(sym_name + 2, symbolName) != NULL && (mangleNameLength = strlen(sym_name)) < mangleNameLastLength)
		{
			mangleNameLastLength = mangleNameLength;
			addr = (char *)(dlmap->l_addr + sym.st_value);
		}
	}

	munmap(file_hdr, dlstat.st_size);

	return addr;
}

int lib_load_info(void *addr, lib_t *lib)
{
	Dl_info info;

	if ((!dladdr(addr, &info) && !info.dli_fbase) || !info.dli_fname) {
		return 0;
	}

	lib->base = (char *)info.dli_fbase;
	lib->size = (uint32)dlsize(info.dli_fbase);
	lib->handle = (char *)dlopen(info.dli_fname, RTLD_NOW);

	return 1;
}