#include "extdll.h"
#include "meta_api.h"
#include "Main.h"

#include "sys_shared.cpp"
#include "interface.cpp"

#undef C_DLLEXPORT

#include <sys/mman.h>
#define C_DLLEXPORT extern "C" __attribute__((visibility("default")))

plugin_info_t Plugin_info = {
	META_INTERFACE_VERSION,
	"AimEspFix",
	"1.0",
	"18/11/17",
	"FrukT & Dev-CS Team",
	"",
	"AIMESPFIX",
	PT_ANYTIME,
	PT_NEVER,
};

meta_globals_t *gpMetaGlobals;
gamedll_funcs_t *gpGamedllFuncs;
mutil_funcs_t *gpMetaUtilFuncs;
enginefuncs_t g_engfuncs;
enginefuncs_t gpEnginefuncs;
enginefuncs_t gpEnginefuncs_Post;
enginefuncs_t *g_pEnginefuncsTable;
enginefuncs_t *g_pEnginefuncsTable_Post;
globalvars_t *gpGlobals;
DLL_FUNCTIONS gpFunctionTable;
DLL_FUNCTIONS gpFunctionTable_Post;
DLL_FUNCTIONS *g_pFunctionTable;
DLL_FUNCTIONS *g_pFunctionTable_Post;
META_FUNCTIONS gMetaFunctionTable;

bool g_hasReHLDS = false;
IRehldsApi* g_RehldsApi;
const RehldsFuncs_t* g_RehldsFuncs;
IRehldsServerData* g_RehldsData;
IRehldsHookchains* g_RehldsHookchains;
IRehldsServerStatic* g_RehldsSvs;

C_DLLEXPORT int GetEntityAPI2(DLL_FUNCTIONS *pFunctionTable, int *)
{
	memset(&gpFunctionTable, 0, sizeof(DLL_FUNCTIONS));

	gpFunctionTable.pfnSpawn = DispatchSpawn;
	gpFunctionTable.pfnPlayerPostThink = PlayerPostThink;

	memcpy(pFunctionTable, &gpFunctionTable, sizeof(DLL_FUNCTIONS));

	g_pFunctionTable = pFunctionTable;

	return TRUE;
}

C_DLLEXPORT int GetEntityAPI2_Post(DLL_FUNCTIONS *pFunctionTable, int *)
{
	memset(&gpFunctionTable_Post, 0, sizeof(DLL_FUNCTIONS));

	gpFunctionTable_Post.pfnClientConnect = ClientConnect_Post;
	gpFunctionTable_Post.pfnClientPutInServer = ClientPutInServer_Post;
	gpFunctionTable_Post.pfnServerActivate = ServerActivate_Post;
	gpFunctionTable_Post.pfnServerDeactivate = ServerDeactivate_Post;
	gpFunctionTable_Post.pfnAddToFullPack = AddToFullPack_Post;
	gpFunctionTable_Post.pfnUpdateClientData = UpdateClientData_Post;

	memcpy(pFunctionTable, &gpFunctionTable_Post, sizeof(DLL_FUNCTIONS));

	g_pFunctionTable_Post = pFunctionTable;

	return TRUE;
}

C_DLLEXPORT int Meta_Query(char *, plugin_info_t **pPlugInfo, mutil_funcs_t *pMetaUtilFuncs)
{
	*pPlugInfo = &(Plugin_info);

	gpMetaUtilFuncs = pMetaUtilFuncs;

	return TRUE;
}

C_DLLEXPORT int GetEngineFunctions(enginefuncs_t *pEnginefuncsTable, int *interfaceVersion)
{
	memset(&gpEnginefuncs, 0, sizeof(enginefuncs_t));
	
	gpEnginefuncs.pfnMessageBegin = MessageBegin;
	gpEnginefuncs.pfnWriteByte = WriteByte;
	gpEnginefuncs.pfnMessageEnd = MessageEnd;

	memcpy(pEnginefuncsTable, &gpEnginefuncs, sizeof(enginefuncs_t));

	g_pEnginefuncsTable = pEnginefuncsTable;

	return TRUE;
}

C_DLLEXPORT int GetEngineFunctions_Post(enginefuncs_t *pEnginefuncsTable, int *interfaceVersion)
{
	memset(&gpEnginefuncs_Post, 0, sizeof(enginefuncs_t));

	gpEnginefuncs_Post.pfnEmitSound = EmitSound_Post;

	memcpy(pEnginefuncsTable, &gpEnginefuncs_Post, sizeof(enginefuncs_t));

	g_pEnginefuncsTable_Post = pEnginefuncsTable;

	return TRUE;
}

C_DLLEXPORT int Meta_Attach(PLUG_LOADTIME now, META_FUNCTIONS *pFunctionTable, meta_globals_t *pMGlobals, gamedll_funcs_t *pGamedllFuncs)
{
	gpMetaGlobals = pMGlobals;
	gpGamedllFuncs = pGamedllFuncs;

	auto st = RehldsApi_Init();

	if (st == RETURN_LOAD)
	{
		g_hasReHLDS = true;
	}
	else
	{
		if (st == RETURN_MAJOR_MISMATCH || st == RETURN_MINOR_MISMATCH) {
			return FALSE;
		}
	}

	OnMetaAttach();

	gMetaFunctionTable.pfnGetEntityAPI2 = GetEntityAPI2;
	gMetaFunctionTable.pfnGetEntityAPI2_Post = GetEntityAPI2_Post;
	gMetaFunctionTable.pfnGetEngineFunctions = GetEngineFunctions;
	gMetaFunctionTable.pfnGetEngineFunctions_Post = GetEngineFunctions_Post;

	memcpy(pFunctionTable, &gMetaFunctionTable, sizeof(META_FUNCTIONS));

	return TRUE;
}

C_DLLEXPORT int Meta_Detach(PLUG_LOADTIME now, PL_UNLOAD_REASON reason)
{
	OnMetaDetach();

	return TRUE;
}

C_DLLEXPORT void WINAPI GiveFnptrsToDll(enginefuncs_t* pengfuncsFromEngine, globalvars_t *pGlobals)
{
	memcpy(&g_engfuncs, pengfuncsFromEngine, sizeof(enginefuncs_t));
	gpGlobals = pGlobals;
}

REHLDS_Status RehldsApi_Init()
{
	CSysModule* engineModule = Sys_LoadModule(/*engine_i486.so*/XorStr<0x21, 15, 0x651237D5>("\x44\x4C\x44\x4D\x4B\x43\x78\x41\x1D\x12\x1D\x02\x5E\x41" + 0x651237D5).s);

	if (!engineModule) {
		return RETURN_NOT_FOUND;
	}

	CreateInterfaceFn ifaceFactory = Sys_GetFactory(engineModule);

	if (!ifaceFactory) {
		return RETURN_NOT_FOUND;
	}

	int retCode = 0;
	g_RehldsApi = (IRehldsApi*)ifaceFactory(VREHLDS_HLDS_API_VERSION, &retCode);

	if (!g_RehldsApi) {
		return RETURN_NOT_FOUND;
	}

	int majorVersion = g_RehldsApi->GetMajorVersion();
	int minorVersion = g_RehldsApi->GetMinorVersion();

	if (majorVersion != REHLDS_API_VERSION_MAJOR)
	{
		AEF_PRINT(/*%s ReHLDS API major version mismatch expected %d real %d\n*/XorStr<0x64, 58, 0xC0FBF1D5>("\x41\x16\x46\x35\x0D\x21\x26\x2F\x3F\x4D\x2F\x3F\x39\x51\x1F\x12\x1E\x1A\x04\x57\x0E\x1C\x08\x08\x15\x12\x10\x5F\xED\xE8\xF1\xEE\xE5\xF1\xE5\xEF\xA8\xEC\xF2\xFB\xE9\xEE\xFA\xEA\xF4\xB1\xB7\xF7\xB4\xE7\xF3\xF6\xF4\xB9\xBF\xFF\x96" + 0xC0FBF1D5).s, LOG_PREFIX, REHLDS_API_VERSION_MAJOR, majorVersion);

		if (majorVersion < REHLDS_API_VERSION_MAJOR)
		{
			AEF_PRINT(/*%s Please update the ReHLDS up to a major version API >= %d\n*/XorStr<0xA1, 61, 0x21B2A974>("\x84\xD1\x83\xF4\xC9\xC3\xC6\xDB\xCC\x8A\xDE\xDC\xC9\xCF\xDB\xD5\x91\xC6\xDB\xD1\x95\xE4\xD2\xF0\xF5\xFE\xE8\x9C\xC8\xCE\x9F\xB4\xAE\xE2\xA2\xE4\xA8\xA7\xAD\xA7\xBB\xEA\xBD\xA9\xBF\xBD\xA6\xBF\xBF\xF2\x92\x84\x9C\xF6\xE9\xE5\xF9\xFF\xBF\xD6" + 0x21B2A974).s, LOG_PREFIX, REHLDS_API_VERSION_MAJOR);
		}
		else if (majorVersion > REHLDS_API_VERSION_MAJOR)
		{
			AEF_PRINT(/*%s Please update the AimEspFix up to a major version API >= %d\n*/XorStr<0x7F, 64, 0x15B8B45C>("\x5A\xF3\xA1\xD2\xEF\xE1\xE4\xF5\xE2\xA8\xFC\xFA\xEF\xED\xF9\xEB\xAF\xE4\xF9\xF7\xB3\xD5\xFC\xFB\xD2\xEB\xE9\xDC\xF2\xE4\xBD\xEB\xEF\x80\xD5\xCD\x83\xC5\x85\xCB\xC6\xC2\xC6\xD8\x8B\xDA\xC8\xDC\xDC\xD9\xDE\xDC\x93\xF5\xE5\xFF\x97\x86\x84\x9A\x9E\xD8\xB7" + 0x15B8B45C).s, LOG_PREFIX, majorVersion);
		}

		return RETURN_MAJOR_MISMATCH;
	}

	if (minorVersion < REHLDS_API_VERSION_MINOR)
	{
		AEF_PRINT(/*%s ReHLDS API minor version mismatch expected at least %d real %d\n*/XorStr<0x44, 67, 0xE69AB67A>("\x61\x36\x66\x15\x2D\x01\x06\x0F\x1F\x6D\x0F\x1F\x19\x71\x3F\x3A\x3A\x3A\x24\x77\x2E\x3C\x28\x28\x35\x32\x30\x7F\x0D\x08\x11\x0E\x05\x11\x05\x0F\x48\x0C\x12\x1B\x09\x0E\x1A\x0A\x14\x51\x13\x07\x54\x19\x13\x16\x0B\x0D\x5A\x5E\x18\x5D\x0C\x1A\xE1\xED\xA2\xA6\xE0\x8F" + 0xE69AB67A).s, LOG_PREFIX, REHLDS_API_VERSION_MINOR, minorVersion);
		AEF_PRINT(/*%s Please update the ReHLDS up to a minor version API >= %d\n*/XorStr<0xF9, 61, 0x62CE22C4>("\xDC\x89\xDB\xAC\x91\x9B\x9E\x73\x64\x22\x76\x74\x61\x67\x73\x6D\x29\x7E\x63\x69\x2D\x5C\x6A\x58\x5D\x56\x40\x34\x60\x66\x37\x6C\x76\x3A\x7A\x3C\x70\x77\x71\x4F\x53\x02\x55\x41\x57\x55\x4E\x47\x47\x0A\x6A\x7C\x64\x0E\x11\x0D\x11\x17\x57\x3E" + 0x62CE22C4).s, LOG_PREFIX, REHLDS_API_VERSION_MINOR);
		
		return RETURN_MINOR_MISMATCH;
	}

	g_RehldsFuncs = g_RehldsApi->GetFuncs();
	g_RehldsData = g_RehldsApi->GetServerData();
	g_RehldsHookchains = g_RehldsApi->GetHookchains();
	g_RehldsSvs = g_RehldsApi->GetServerStatic();

	return RETURN_LOAD;
}
