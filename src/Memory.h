#ifndef _MEMORY_H_
#define _MEMORY_H_

#include "Main.h"

#include <sys/mman.h>
#include <link.h>
#include <fcntl.h>
#include <netinet/in.h>

typedef int SOCKET;

#define Align(addr)		((void *)(((dword)addr) & ~(sysconf(_SC_PAGESIZE) - 1)))

#define DWORD_GET_ADDR_FUNC(x, f)		\
	((dword)f - (dword)x - 5)

#pragma pack(push, 1)
struct jmp_t
{
	unsigned char _jmp;
	void *addr;
};
#pragma pack(pop)

struct lib_t
{
	char *base;
	size_t size;
	char *handle;
};

struct hooks_t
{
	void *addr[20];
	int patchSize;
	void* originalBytes[20][sizeof(jmp_t)];
};

template<typename T>
size_t GET_FUNC_PTR_CAST(T pfunc)
{
	return reinterpret_cast<size_t &>(pfunc);
}

extern bool Mem_UnPatch(hooks_t *a);
extern bool Mem_MemCpy(void *addr, void *patch, int len);
extern char *lib_find_symbol(lib_t *lib, const char *symbolName);
extern int lib_load_info(void *addr, lib_t *lib);

typedef void(*PFN_PM_PlaySound_HLDS)(int channel, const char *sample, float volume, float attn, int flags, int pitch);

#endif //_MEMORY_H_
