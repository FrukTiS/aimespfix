#ifndef _MAIN_H
#define _MAIN_H

#include "sys_shared.h"
#include "interface.h"
#include "extdll.h"
#include "cbase.h"
#include "meta_api.h"
#include "pm_defs.h"
#include "entity_state.h"
#include "rehlds_api.h"
#include "Memory.h"

template <int XORSTART, int BUFLEN, int XREFKILLER>
class XorStr
{
	private:
		XorStr();

	public:
		char s[BUFLEN];
		XorStr(const char* xs);
};

template <int XORSTART, int BUFLEN, int XREFKILLER>
XorStr<XORSTART, BUFLEN, XREFKILLER>::XorStr(const char* xs)
{
	int xvalue = XORSTART;
	for (int i = 0; i < (BUFLEN - 1); i++)
	{
		s[i] = xs[i - XREFKILLER] ^ xvalue;
		xvalue += 1;
		xvalue %= 256;
	}
	s[BUFLEN - 1] = 0;
}

#define OFFSET_TEAM_ID				119
#define OFFSET_BLIND_START_TIME		520
#define OFFSET_BLIND_HOLD_TIME		521
#define OFFSET_BLIND_ALPHA			523
#define OFFSET_C4					793
#define OFFSET_VIP					857
#define OFFSET_CANSHOOT				896

typedef unsigned char uint8_t;
typedef unsigned int uint32_t;
typedef uint32_t bool32_t;
typedef unsigned long dword;
typedef unsigned char byte;
typedef float float_precision;

#define MAX_CLIENTS							32
#define SVC_SOUND							6
#define DEFAULT_SOUND_PACKET_VOLUME			255
#define DEFAULT_SOUND_PACKET_ATTENUATION	1.0f
#define DEFAULT_SOUND_PACKET_PITCH			100
#define GET_FOR_C4(e)				(*((uint8_t *)e->pvPrivateData + OFFSET_C4))
#define GET_FOR_ISVIP(e)			(*((uint8_t *)e->pvPrivateData + OFFSET_VIP))
#define GET_BLIND_ALPHA(e)			(*((int *)e->pvPrivateData + OFFSET_BLIND_ALPHA))
#define GET_BLIND_START_TIME(e)		(*((float *)e->pvPrivateData + OFFSET_BLIND_START_TIME))
#define GET_BLIND_HOLD_TIME(e)		(*((float *)e->pvPrivateData + OFFSET_BLIND_HOLD_TIME))
#define NUM_FOR_TEAM_ID(e)			(*((int *)e->pvPrivateData + OFFSET_TEAM_ID))
#define NUM_FOR_EDICT(e)			((int)(e - NullEdict))
#define EDICT_NUM(e)				((edict_t *)(NullEdict + e))
#define SCORE_STATUS_DEAD			(1<<0)
#define SCORE_STATUS_BOMB			(1<<1)
#define SCORE_STATUS_VIP			(1<<2)
#define LOG_PREFIX					/*[AimEspFix]:*/XorStr<0x8F,13,0x4319AB6E>("\xD4\xD1\xF8\xFF\xD6\xE7\xE5\xD0\xFE\xE0\xC4\xA0"+0x4319AB6E).s

typedef struct ModuleCfg_s
{
	int AimFFA;
	int AimTabFix;
	int AimTabFlashedFix;
	int SndActive;
	int SndCount;
	int SndScalez;
	int SndRadius;
	int SndVolume;
	int SndOnlyPlayer;
} ModuleCfg_t;

int Load_Config();
int Parse_Config(const char *path);
int DispatchSpawn(edict_t *pEntity);
int AddToFullPack_Post(entity_state_s *state, int pE, edict_t *pEntity, edict_t *pHost, int pHostFlags, int Player, unsigned char *pSet);
void ClientPutInServer_Post(edict_t *pEdict);
void PlayerPostThink(edict_t *pEdict);
void UpdateClientData_Post(const struct edict_s *pEnt, int sendweapons, struct clientdata_s *cd);
void ServerActivate_Post(edict_t *pEdictList, int edictCount, int clientMax);
void MessageBegin(int msg_dest, int msg_type, const float *pOrigin, edict_t *ed);
void PM_Move(struct playermove_s *pm, qboolean server);
void EmitSound_Post(edict_t *entity, int channel, const char *sample, float volume, float attenuation, int fFlags, int pitch);
void PM_PlaySound_Hook(int channel, const char *sample, float volume, float attn, int flags, int pitch);
void GenericPlaySoundHandler(edict_t *entity, const char *sample, float volume, float attn, int pitch);
void SetScoreAttr(edict_t *ed);
void ServerDeactivate_Post();
void WriteByte(int iValue);
void WriteBit(bool Bit);
void WriteBits(int Num, int BitCount);
void WriteBitCoord(float Coord);
void WriteBitVector(Vector Vec);
void FlushBits();
void MessageEnd();
void ResetBits();
void Print_Settings();
void OnMetaAttach();
void OnMetaDetach();
void SV_AimEspFix_Option();
void ModuleActivated();
void StartFrame_Post();
void AEF_PRINT(const char *fmt, ...);
bool ParseCfgParam(const char* param, const char* value);
BOOL ClientConnect_Post(edict_t *pEdict, const char *pszName, const char *pszAddress, char szRejectReason[128]);

enum
{
	SND_FL_VOLUME = BIT(0),			// send volume
	SND_FL_ATTENUATION = BIT(1),	// send attenuation
	SND_FL_LARGE_INDEX = BIT(2),	// send sound number as short instead of byte
	SND_FL_PITCH = BIT(3),			// send pitch
	SND_FL_SENTENCE = BIT(4),		// set if sound num is actually a sentence num
	SND_FL_STOP = BIT(5),			// stop the sound
	SND_FL_CHANGE_VOL = BIT(6),		// change sound vol
	SND_FL_CHANGE_PITCH = BIT(7),	// change sound pitch
	SND_FL_SPAWNING = BIT(8)		// we're spawning, used in some cases for ambients (not sent across network)
};

enum REHLDS_Status {
	RETURN_LOAD,
	RETURN_MINOR_MISMATCH,
	RETURN_MAJOR_MISMATCH,
	RETURN_NOT_FOUND
};

extern bool g_hasReHLDS;
extern IRehldsApi* g_RehldsApi;
extern const RehldsFuncs_t* g_RehldsFuncs;
extern IRehldsServerData* g_RehldsData;
extern IRehldsHookchains* g_RehldsHookchains;
extern IRehldsServerStatic* g_RehldsSvs;
extern REHLDS_Status RehldsApi_Init();
extern ModuleCfg_t g_AimEspFixCfg;
extern DLL_FUNCTIONS *g_pFunctionTable;
extern DLL_FUNCTIONS *g_pFunctionTable_Post;
extern enginefuncs_t *g_pEnginefuncsTable_Post;

#endif //_MAIN_H
